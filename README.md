## Java desktop GUI for Gitlab Notifications

This simple tool runs as a java-tray icon and shows you a list of incoming new merge requests, which are created in repositories, that you have starred (= marked as your favourites). The tray icon changes from a green inbox symbol to a red notification, if a new MR has been opened. By clicking on this MRs from within the popup-menu you can easily navigate to them via the browser-popup.

### Prerequisites
- You have to set the enviroment variable GITLAB_API_KEY with your [personal gitlab access token](https://gitlab.com/-/profile/personal_access_tokens) (only read_* scopes!)

### Running from the IDE
- `mvn clean package`
- run the Starter class

### Running from the [binary distribution](https://gitlab.com/deemkeen/gitlab-notifications/-/releases)
- unpack the zip or tar.gz package file from the latest release
- run the `app` command out of the bin folder

### (Optional) Releasing a new version:
- make sure than [SDKMAN](https://sdkman.io/) is installed
- build a new version with maven
- run `sdk install jreleaser`
- update jreleaser.yml with the new release info
- set following env-vars:
    - JRELEASER_GITLAB_TOKEN (Full Access Gitlab Api Token for this project)
    - JRELEASER_ARTIFACTORY_JFROG_USERNAME (JFrog Username)
    - JRELEASER_ARTIFACTORY_JFROG_PASSWORD (JFrog Password)
- run `jreleaser release`
