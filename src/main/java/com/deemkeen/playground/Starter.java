package com.deemkeen.playground;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import org.gitlab4j.api.Constants.MergeRequestState;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.MergeRequestFilter;
import org.gitlab4j.api.models.Project;

public class Starter {

  public static final String GITLAB_URL = "https://gitlab.com";
  public static final int INITIAL_DELAY = 5000;
  public static final int PERIOD = 5000;

  public static final AtomicBoolean initialRun = new AtomicBoolean(true);

  public static void main(String[] args) {

    final HashSet<Integer> allMrIds = new HashSet<>();
    final TrayGui trayGui = new TrayGui();

    // Gitlab Login
    final String apiKey = System.getenv("GITLAB_API_KEY");

    if (apiKey == null || apiKey.isEmpty()) {
      System.out.println("Exiting: GITLAB_API_KEY not set!");
      System.exit(0);
    }

    GitLabApi gitLabApi = new GitLabApi(GITLAB_URL, apiKey);

    new Timer()
        .schedule(
            new TimerTask() {
              @Override
              public void run() {
                try {
                  notifyForNewMRs(allMrIds, gitLabApi, trayGui);
                } catch (GitLabApiException e) {
                  throw new RuntimeException(e);
                }
              }
            },
            INITIAL_DELAY,
            PERIOD);
  }

  private static void notifyForNewMRs(
      HashSet<Integer> allMrIds, GitLabApi gitLabApi, TrayGui trayGui) throws GitLabApiException {
    final MergeRequestFilter filter = new MergeRequestFilter();
    filter.setCreatedAfter(yesterday());

    HashMap<Integer, Project> starredProjectsById =
        gitLabApi.getProjectApi().getStarredProjects().stream()
            .collect(Collectors.toMap(Project::getId, p -> p, (a, b) -> b, HashMap::new));

    final List<MergeRequest> mrs =
        starredProjectsById.values().parallelStream()
            .map(
                p -> {
                  try {
                    List<MergeRequest> mrList =
                        gitLabApi
                            .getMergeRequestApi()
                            .getMergeRequests(p.getId(), MergeRequestState.OPENED);
                    return mrList;
                  } catch (GitLabApiException e) {
                    System.out.println(e);
                  }
                  return null;
                })
            .filter(Objects::nonNull)
            .flatMap(Collection::stream)
            .filter(mergeRequest -> mergeRequest.getUpdatedAt().after(yesterday()))
            .collect(Collectors.toList());

    final List<Integer> newMrIds =
        mrs.stream().map(MergeRequest::getId).collect(Collectors.toList());

    // Add new
    newMrIds.removeAll(allMrIds);
    allMrIds.addAll(newMrIds);

    final HashSet<MergeRequest> newMRs = new HashSet<>();

    mrs.forEach(
        m -> {
          if (newMrIds.contains(m.getId())) {
            newMRs.add(m);
          }
        });

    // notify if new
    if (!initialRun.get()) {
      for (MergeRequest mergeRequest : newMRs) {
        trayGui.notificate(
            mergeRequest.getTitle(),
            getProjectNameById(starredProjectsById, mergeRequest),
            mergeRequest.getWebUrl());
      }
    } else {
      initialRun.set(false);
    }
  }

  private static String getProjectNameById(
      HashMap<Integer, Project> projectsById, MergeRequest mergeRequest) {
    return projectsById.get(mergeRequest.getProjectId()).getName();
  }

  private static Date yesterday() {
    final Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -1);
    return cal.getTime();
  }
}
