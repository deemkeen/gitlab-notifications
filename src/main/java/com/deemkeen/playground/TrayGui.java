package com.deemkeen.playground;

import dorkbox.systemTray.Menu;
import dorkbox.systemTray.MenuItem;
import dorkbox.systemTray.Separator;
import dorkbox.systemTray.SystemTray;

import java.awt.Desktop;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.atomic.AtomicInteger;

public class TrayGui {

  public static final String GITLAB_NOTIFICATIONS = "Gitlab notifications";
  public static final URL GITLAB_NOTIFICATIONS_ICO = TrayGui.class.getResource("/logo.png");
  public static final URL GITLAB_NOTIFICATIONS_ALERT = TrayGui.class.getResource("/alert.png");
  public static final int MAX_MR_ENTRIES = 10;

  private int initialSize;
  private AtomicInteger currentSize;
  private SystemTray systemTray;

  public TrayGui() {
    try {

      // macOS workaround
      if (System.getProperty("os.name").startsWith("Mac")) {
        SystemTray.FORCE_TRAY_TYPE = SystemTray.TrayType.Awt;
      }

      systemTray = SystemTray.get(GITLAB_NOTIFICATIONS);
      systemTray.setStatus(GITLAB_NOTIFICATIONS);
      systemTray.setImage(GITLAB_NOTIFICATIONS_ICO);
      if (systemTray == null) {
        throw new RuntimeException("Unable to load SystemTray!");
      }

      initializeMenu();

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private void initializeMenu() {
    systemTray
        .getMenu()
        .add(
            new MenuItem(
                "Quit",
                e -> {
                  systemTray.shutdown();
                  System.exit(0);
                }));

    systemTray.getMenu().add(new Separator());
    final MenuItem mergeRequests = new MenuItem("Latest Merge Requests:");
    mergeRequests.setEnabled(false);
    systemTray.getMenu().add(mergeRequests);

    initialSize = systemTray.getMenu().getEntries().size();
    currentSize = new AtomicInteger(0);
  }

  public void notificate(String title, String text, String url) {

    final Menu menu = systemTray.getMenu();
    removeIfObsolete(menu);

    final String itemText = "[" + text + "]\t" + title;
    systemTray.setImage(GITLAB_NOTIFICATIONS_ALERT);

    ActionListener openLink =
        event -> {
          try {
            final MenuItem entry = (MenuItem) event.getSource();
            Desktop.getDesktop().browse(URI.create(url));
            removeSeen(entry);
          } catch (IOException ex) {
            ex.printStackTrace();
          }
        };

    final MenuItem mrMenuItem = new MenuItem(itemText);
    mrMenuItem.setCallback(openLink);

    menu.add(mrMenuItem, initialSize - 1);
    currentSize.incrementAndGet();
  }

  private void removeIfObsolete(Menu menu) {
    if (currentSize.get() >= MAX_MR_ENTRIES) {
      menu.get(currentSize.get()).remove();
      currentSize.decrementAndGet();
    }
  }

  private void removeSeen(MenuItem entry) {
    entry.remove();
    currentSize.decrementAndGet();
    if (currentSize.get() == 0) {
      systemTray.setImage(GITLAB_NOTIFICATIONS_ICO);
    }
  }
}
